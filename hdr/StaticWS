; Copyright 2009 Castle Technology Ltd
;
; Licensed under the Apache License, Version 2.0 (the "License");
; you may not use this file except in compliance with the License.
; You may obtain a copy of the License at
;
;     http://www.apache.org/licenses/LICENSE-2.0
;
; Unless required by applicable law or agreed to in writing, software
; distributed under the License is distributed on an "AS IS" BASIS,
; WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
; See the License for the specific language governing permissions and
; limitations under the License.
;

        GET     hdr.SDMA
        GET     hdr.Video
        GET     hdr.Audio
        GET     hdr.I2C
        GET     hdr.board
        GET     hdr.NIC
        GET     hdr.CPUClk
        GET     Hdr:GPIODevice
        GET     Hdr:SDHCIDevice
        GET     Hdr:BMUDevice
        GET     Hdr:RTCDevice

sb              RN      9

        MACRO
        CallOS  $entry, $tailcall
        ASSERT  $entry <= HighestOSEntry
 [ "$tailcall"=""
        MOV     lr, pc
 |
   [ "$tailcall"<>"tailcall"
        ! 0, "Unrecognised parameter to CallOS"
   ]
 ]
        LDR     pc, OSentries + 4*$entry
        MEND

; Per-SDHCI workspace

                ^       0
SDHCIDevice     #       HALDevice_SDHCISize          ; see Hdr:SDHCIDevice
SDHCISB         #       4                            ; pointer to HAL workspace for HAL calls
SDHCISlotInfo   #       HALDeviceSDHCI_SlotInfo_Size ; each of our controllers has just the 1 slot
SDHCISize       *       :INDEX:@

MaxSDControllers *      3

; Per-BMU workspace

                ^       0
BMUDevice       #       HALDevice_BMU_Size           ; see Hdr:BMUDevice
BMUWS           #       4                            ; pointer to HAL workspace for HAL calls
BMUParams       #       4                            ; Per-device params
BMUSize         *       :INDEX:@

; Per-RTC workspace

                ^       0
RTCDevice       #       HALDevice_RTC_Size           ; see Hdr:RTCDevice
RTCDeviceHAL_SB #       4                            ; pointer to HAL workspace for HAL calls
RTCSize         *       :INDEX:@

                ^       0,sb
BoardConfig     #       BoardConfig_Size ; NOTE: Almost all code assumes the board config is at the start. You have been warned!
OSheader        #       4
OSentries       #       4*(HighestOSEntry+1)
L3_Log          #       4 ; L3 base logical address
L4_Core_Log     #       4 ; L4 Core/Wakeup base logical address
L4_Wakeup_Log   #       4 ; L4 Wakeup base logical address
L4_Per_Log      #       4 ; L4 Per base logical address
MPU_INTC_Log    #       4 ; MPU_INTC logical address
Timers_Log      #       4 ; Timers logical base address
L4_ClockMan_Log #       4 ; L4_ClockMan base address
L4_PowerMan_Log #       4 ; L4_PowerMan base address
L4_32KTIMER_Log #       4 ; L4_32KTIMER base address
L4_USBTLL_Log   #       4 ; L4_USBTLL base address
L4_USB_Host_Log #       4 ; L4_USB_Host base address
L4_USB_OTG_Log  #       4 ; L4_USB_OTG base address
GPMC_Regs_Log   #       4 ; GPMC_Regs base address
sys_clk         #       4 ; System clock speed in Hz
Timer_DelayMul  #       4 ; sys_clk/100KHz
HALInitialised  #       4 ; Flag for whether HAL_Init is done yet
NVMemoryFound   #       4 ; Size of EEPROM detected (may be 0)
L4_Display_Log  #       4 ; L4_Display base address
L4_sDMA_Log     #       4 ; L4_sDMA logical address
IntSRAM_Log     #       4 ; SRAM logical address

L4_GPIO_Table   #       0 ; Lookup table for GPIO addresses
L4_GPIO1_Log    #       4 ; L4_GPIO1 base address
L4_GPIO2_Log    #       4 ; L4_GPIO2 base address
L4_GPIO3_Log    #       4 ; L4_GPIO3 base address
L4_GPIO4_Log    #       4 ; L4_GPIO4 base address
L4_GPIO5_Log    #       4 ; L4_GPIO5 base address
L4_GPIO6_Log    #       4 ; L4_GPIO6 base address

I2C_Table       #       0 ; Table of I2C HW ptrs & transfer states
I2C1Block       #       I2CBlockSize
I2C2Block       #       I2CBlockSize
I2C3Block       #       I2CBlockSize

 [ DebugInterrupts
LastInterrupt_IRQ #     4 ; Last IRQ, -1 if cleared
LastInterrupt_FIQ #     4 ; Last FIQ, -1 if cleared
  [ ExtraDebugInterrupts
ExtraDebugIRQEnabled #  4 ; Nonzero if extra debugging enabled
  ]
 ]

UARTFCRSoftCopy #       4
NCNBWorkspace   #       4 ; Base of ncnb workspace
NCNBAllocNext   #       4 ; next free address in ncnb workspace

DMAPktSz_Audio  #       4 ; DMA packet size to use for audio transfers (McBSP2 TX)
KbdMatrixScan   #       4 ; Scan results for boot-time keyboard matrix scanning
KbdMatrixTime   #       4 ; Start time of boot-time keyboard matrix scanning

SDMAWS          #       SDMA_WorkspaceSize

VideoDevice     #       Video_DeviceSize
VideoBoardConfig #      VideoBoardConfig_Size

AudioWS         #       Audio_WorkspaceSize
NICWS           #       NIC_DeviceSize
CPUClkWS        #       CPUClk_WorkspaceSize
GPIOWS          #       6 * (HALDevice_GPIO_Size_1_0 + (2*4))
NVRAMWS         #       HALDeviceSize
RTCWS           #       RTCSize

SDIOWS          #       SDHCISize * MaxSDControllers
BMUWS1          #       BMUSize
BMUWS2          #       BMUSize

                #       (((:INDEX:@)+15):AND::NOT:15)-(:INDEX:@)

HAL_WsSize      *       :INDEX:@

        END
