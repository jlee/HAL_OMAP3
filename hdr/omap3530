; Copyright 2009 Castle Technology Ltd
;
; Licensed under the Apache License, Version 2.0 (the "License");
; you may not use this file except in compliance with the License.
; You may obtain a copy of the License at
;
;     http://www.apache.org/licenses/LICENSE-2.0
;
; Unless required by applicable law or agreed to in writing, software
; distributed under the License is distributed on an "AS IS" BASIS,
; WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
; See the License for the specific language governing permissions and
; limitations under the License.
;

          ; Debugging in the serial port (UART 3)
          GBLL    Debug
Debug     SETL    {FALSE}

          ; Boot timings using the 32KHz timer
          GBLL    DebugTiming
DebugTiming SETL  Debug :LAND: {FALSE}

          ; Should the I cache be off when the MMU is
          GBLL    CacheOff
CacheOff  SETL    {FALSE}

          ; QEMU support - disables some code that does stuff QEMU doesn't support
          GBLL    QEMU
QEMU      SETL    {FALSE}

          ; Interrupt debugging - warn over serial port when IRQSource/FIQSource is called twice in a row without IRQClear/FIQClear being called inbetween
          GBLL    DebugInterrupts
DebugInterrupts   SETL  Debug :LAND: {TRUE}

          ; Extra interrupt debugging - when a missed IRQClear is detected, enables code that prints a trace of IRQClear and IRQSource calls. Note: Doesn't track FIQs at the moment!
          GBLL    ExtraDebugInterrupts
ExtraDebugInterrupts    SETL DebugInterrupts :LAND: {FALSE}


; Physical memory map. All unmentioned ranges are reserved.
; 00000000-3FFFFFFF GPMC
; 40000000-4001BFFF Internal boot ROM
; 40200000-4020FFFF Internal SRAM (64K)
; 48000000-48FFFFFF L4-Core
;(48300000-4833FFFF L4-Wakeup)
; 49000000-490FFFFF L4-Per
; 50000000-53FFFFFF SGX
; 54000000-547FFFFF L4-Emu
; 5C000000-5EFFFFFF IVA2.2
; 68000000-68FFFFFF L3 Control registers
; 6C000000-6CFFFFFF SMS registers
; 6D000000-6DFFFFFF SDRC registers
; 6E000000-6EFFFFFF GPMC registers
; 70000000-7FFFFFFF SDRC-SMS virtual address space 0
; 80000000-BFFFFFFF CS0/CS1-SDRAM
; E0000000-FFFFFFFF SDRC-SMS virtual address space 1

IntSRAM_Base       *        &40200000
IntSRAM_Size       *        &00010000

L4_Core            *        &48000000
L4_Wakeup          *        &48300000
L4_Per             *        &49000000
SGX                *        &50000000
L4_Emu             *        &54000000
IVA22              *        &5C000000
L3_Control         *        &68000000
SMS_Regs           *        &6C000000
SDRC_Regs          *        &6D000000
GPMC_Regs          *        &6E000000
SDRC_SMS_0         *        &70000000
CS0_SDRAM          *        &80000000 ; CS1 SDRAM is at a configurable offset from here
SDRC_SMS_1         *        &E0000000

MPU_INTC           *        &48200000 ; Lies inside the L4-Core address range, but isn't actually on the interconnect
MPU_INTC_SIZE      *             4096

; L3 locations
L3_RT              *      L3_Control+&00000 ; L3 config registers
L3_SI              *      L3_Control+&00400 ; Sideband signals config
MPU_SS_IA          *      L3_Control+&01400 ; MPU initiator config
IVA22_SS_IA        *      L3_Control+&01800 ; IVA2.2 initiator config
SGX_SS_IA          *      L3_Control+&01C00 ; SGX initiator config
SMS_TA             *      L3_Control+&02000 ; SMS target config
GPMC_TA            *      L3_Control+&02400 ; GPMC target config
OCM_RAM_TA         *      L3_Control+&02800 ; OCM RAM target config
OCM_ROM_TA         *      L3_Control+&02C00 ; OCM ROM target config
USB_HOST_IA        *      L3_Control+&04000 ; USB host initiator config
USB_OTG_IA         *      L3_Control+&04400 ; USB OTG initiator config
sDMA_RD_IA         *      L3_Control+&04C00 ; sDMA RD initiator config
sDMA_WR_IA         *      L3_Control+&05000 ; sDMA WR initiator config
DISPLAY_SS_IA      *      L3_Control+&05400 ; Display initiator config
CAMERA_ISP_IA      *      L3_Control+&05800 ; Camera ISP initiator config
DAP_IA             *      L3_Control+&05C00 ; Debug access port initiator config
IVA22_SS_TA        *      L3_Control+&06000 ; IVA2.2 target config
SGX_SS_TA          *      L3_Control+&06400 ; SGX target config
L4_Core_TA         *      L3_Control+&06800 ; L4 target config
L4_Per_TA          *      L3_Control+&06C00 ; L4 target config
RT_PM              *      L3_Control+&10000 ; Register target port protection
GPMC_PM            *      L3_Control+&12400 ; GPMC target port protection
OCM_RAM_PM         *      L3_Control+&12800 ; OCM RAM target port protection
OCM_ROM_PM         *      L3_Control+&12C00 ; OCM ROM target port protection
IVA22_PM           *      L3_Control+&14000 ; IVA2.2 target port protection
L3_Size            *                 &14400 ; Size we request to be mapped in

; L4-Core locations
L4_Control         *      L4_Core+&02000 ; Control
L4_PadConf         *      L4_Core+&02030 ; Pad multiplexing
L4_CtrlDevStatus   *      L4_Core+&0244C ; Control device status register
L4_PBiasLite       *      L4_Core+&02520 ; Extended drain i/o control
L4_CtrlWkupCtrl    *      L4_Core+&02A5C ; Control wakeup control register
L4_SysCtrlModule   *      L4_Core+&02FFF ; System control module thing
L4_ClockMan        *      L4_Core+&04000 ; Clock manager
L4_CoreConfig      *      L4_Core+&40000 ; L4 core config
L4_Display         *      L4_Core+&50000 ; Display subsystem config
L4_sDMA            *      L4_Core+&56000 ; sDMA config
L4_I2C3            *      L4_Core+&60000 ; I2C #3
L4_USBTLL          *      L4_Core+&62000 ; USBTLL thing
L4_USB_Host        *      L4_Core+&64000 ; USB host config
L4_UART1           *      L4_Core+&6A000 ; UART 1
L4_UART2           *      L4_Core+&6C000 ; UART 2
L4_I2C1            *      L4_Core+&70000 ; I2C #1
L4_I2C2            *      L4_Core+&72000 ; I2C #2
L4_McBSP1          *      L4_Core+&74000 ; McBSP1 thing
L4_GPTIMER10       *      L4_Core+&86000 ; Timer config
L4_GPTIMER11       *      L4_Core+&88000 ; Timer config
L4_MAILBOX         *      L4_Core+&94000 ; IPC mailbox
L4_McBSP5          *      L4_Core+&96000 ; McBSP5 thing (MIDI)
L4_McSPI1          *      L4_Core+&98000 ; McSPI1 thing
L4_McSPI2          *      L4_Core+&9A000 ; McSPI2 thing
L4_MemCard1        *      L4_Core+&9C000 ; MMC/SD/SDIO 1 config
L4_USB_OTG         *      L4_Core+&AB000 ; USB OTG config
L4_MemCard3        *      L4_Core+&AD000 ; MMC/SD/SDIO 3 config
L4_HDQ_1Wire       *      L4_Core+&B2000 ; HDQ/1-wire bus config
L4_MemCard2        *      L4_Core+&B4000 ; MMC/SD/SDIO 2 config
L4_ICR_MPU         *      L4_Core+&B6000 ; ICR MPU port thing
L4_McSPI3          *      L4_Core+&B8000 ; McSPI3 thing
L4_McSPI4          *      L4_Core+&BA000 ; McSPI4 thing
L4_Camera_ISP      *      L4_Core+&BC000 ; Camera ISP thing
L4_Modem_INTC      *      L4_Core+&C7000 ; Modem & INTC
L4_SR1             *      L4_Core+&C9000 ; SR1 thing
L4_SR2             *      L4_Core+&CB000 ; SR2 thing
L4_ICR_Modem       *      L4_Core+&CD000 ; ICR modem port
L4_Core_Size       *              &CF000 ; Size we request to be mapped in

; L4-Wakeup locations
L4_PowerMan        *      L4_Wakeup+&06000 ; Power and reset manager
L4_CONTROL_IDCODE  *      L4_Wakeup+&0A204 ; OMAP revision code
L4_PRODID          *      L4_Wakeup+&0A20C ; PROD/SKU ID
L4_DIE_ID          *      L4_Wakeup+&0A218 ; 128 bit unique ID
L4_GPIO1           *      L4_Wakeup+&10000 ; GPIO 1
L4_WDTIMER2        *      L4_Wakeup+&14000 ; WDTIMER 2
L4_GPTIMER1        *      L4_Wakeup+&18000 ; GPTIMER 1
L4_32KTIMER        *      L4_Wakeup+&20000 ; 32K timer
L4_WakeConfig      *      L4_Wakeup+&28000 ; L4-wakeup config
L4_Wakeup_Size     *                &41000 ; Size we request to be mapped in

; L4-Peripheral locations
L4_PerConfig     *   L4_Per+&00000 ; L4-Per config
L4_UART3         *   L4_Per+&20000 ; UART 3
L4_McBSP2        *   L4_Per+&22000 ; McBSP2 thing (Audio for codec)
L4_McBSP3        *   L4_Per+&24000 ; McBSP3 thing (Bluetooth voice data)
L4_McBSP4        *   L4_Per+&26000 ; McBSP4 thing (digital baseband voice data)
L4_McBSP2S       *   L4_Per+&28000 ; McBSP2 thing (sidetone)
L4_McBSP3S       *   L4_Per+&2A000 ; McBSP3 thing (sidetone)
L4_WDTIMER3      *   L4_Per+&30000 ; WDTIMER 3
L4_GPTIMER2      *   L4_Per+&32000 ; GPTIMER 2
L4_GPTIMER3      *   L4_Per+&34000 ; GPTIMER 3
L4_GPTIMER4      *   L4_Per+&36000 ; GPTIMER 4
L4_GPTIMER5      *   L4_Per+&38000 ; GPTIMER 5
L4_GPTIMER6      *   L4_Per+&3A000 ; GPTIMER 6
L4_GPTIMER7      *   L4_Per+&3C000 ; GPTIMER 7
L4_GPTIMER8      *   L4_Per+&3E000 ; GPTIMER 8
L4_GPTIMER9      *   L4_Per+&40000 ; GPTIMER 9
L4_UART4         *   L4_Per+&42000 ; UART 4 (AMDM37x)
L4_GPIO2         *   L4_Per+&50000 ; GPIO 2
L4_GPIO3         *   L4_Per+&52000 ; GPIO 3
L4_GPIO4         *   L4_Per+&54000 ; GPIO 4
L4_GPIO5         *   L4_Per+&56000 ; GPIO 5
L4_GPIO6         *   L4_Per+&58000 ; GPIO 6
L4_Per_Size      *          &5A000 ; Size we request to be mapped in

; CONTROL_IDCODE fields:
; bits 31-28 give the silicon revision
; bits 27-12 give the hawkeye number (OMAP family)
; bits 11-0 are 0x02f
HAWKEYE_OMAP35x_ES10  *   &B6D6 ; spruf98g. Also OMAP34x ES1.0?
HAWKEYE_OMAP35x       *   &B7AE ; spruf98g. Also OMAP34x >ES1.0?
HAWKEYE_AMDM37x       *   &B891 ; sprugn4
HAWKEYE_AM35x         *   &B868 ; sprugr0

                            ; OMAP34x? OMAP35x? AMDM37x? AM35x?
REVISION_ES10         *   0 ; x        x        x        x
REVISION_ES20         *   1 ; x        x
REVISION_ES21         *   2 ; x        x
REVISION_ES30         *   3 ; x        x
REVISION_ES31         *   4 ; x        x
REVISION_ES311        *   5 ; x
REVISION_ES312        *   7 ; x        x
REVISION_ES11         *   1 ;                   x
REVISION_ES12         *   2 ;                   x

      [ Debug :LAND: :LNOT: :DEF: DebugExported
        IMPORT  DebugHALPrint
        IMPORT  DebugHALPrintReg
        IMPORT  DebugHALPrintByte
        IMPORT  DebugCallstack
        IMPORT  DebugMemDump
      ]

        MACRO
        DebugChar $temp1,$temp2,$char
      [ Debug :LAND: {FALSE}
        LDR     $temp1,[sb, #BoardConfig_DebugUART]
        MOV     $temp2,#$char
        STRB    $temp2,[$temp1]
      ]
        MEND

        MACRO
        DebugTX $str
      [ Debug
        BL      DebugHALPrint
        =       "$str", 13, 10, 0
        ALIGN
      ]
        MEND

        MACRO
        DebugReg $reg, $str
      [ Debug
        Push   "$reg"
        [ "$str" <> ""
        BL     DebugHALPrint
        =      "$str",0
        ALIGN
        ]
        BL     DebugHALPrintReg
      ]
        MEND

        MACRO
        DebugRegByte $reg, $str
      [ Debug
        Push   "$reg"
        [ "$str" <> ""
        BL     DebugHALPrint
        =      "$str",0
        ALIGN
        ]
        BL     DebugHALPrintByte
      ]
        MEND

        MACRO
        DebugTime $temp,$str
      [ DebugTiming
        LDR     $temp,L4_32KTIMER_Log
        LDR     $temp,[$temp,#16] ; REG_32KSYNCNT_CR
        DebugReg $temp, "$str"
      ]
        MEND

        MACRO
        DebugTimeNoMMU $temp,$str
      [ DebugTiming
        LDR     $temp,=&48320000
        LDR     $temp,[$temp,#16]
        DebugReg $temp, "$str"
      ]
        MEND

        END
