; Copyright 2012 Castle Technology Ltd
;
; Licensed under the Apache License, Version 2.0 (the "License");
; you may not use this file except in compliance with the License.
; You may obtain a copy of the License at
;
;     http://www.apache.org/licenses/LICENSE-2.0
;
; Unless required by applicable law or agreed to in writing, software
; distributed under the License is distributed on an "AS IS" BASIS,
; WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
; See the License for the specific language governing permissions and
; limitations under the License.
;

; SmartReflex registers - relative to L4_SR1/L4_SR2

SR37x_SRCONFIG            * &00
SR37x_SRSTATUS            * &04
SR37x_SENVAL              * &08
SR37x_SENMIN              * &0C
SR37x_SENMAX              * &10
SR37x_SENAVG              * &14
SR37x_AVGWEIGHT           * &18
SR37x_NVALUERECIPROCAL    * &1C
SR37x_IRQSTATUS_RAW       * &24
SR37x_IRQSTATUS           * &28
SR37x_IRQENABLE_SET       * &2C
SR37x_IRQENABLE_CLR       * &30
SR37x_SENERROR_REG        * &34
SR37x_ERRCONFIG           * &38

; eFuse registers containing SmartReflex parameters - relative to L4_Core

CONTROL_FUSE_OPP50_VDD1   * &2384
CONTROL_FUSE_OPP100_VDD1  * &2388
CONTROL_FUSE_OPP130_VDD1  * &2390
CONTROL_FUSE_OPP1G_VDD1   * &2380
CONTROL_FUSE_OPP50_VDD2   * &2398
CONTROL_FUSE_OPP100_VDD2  * &239C
CONTROL_FUSE_SR           * &23A0

; IRQs

SR1_IRQ  * 18
SR2_IRQ  * 19

; Fixed values from Linux sources
SR37x_ACCUMDATA     * &1F4 ; 5ms accumulator time window
SR37x_SENNAVGWEIGHT * 1
SR37x_SENPAVGWEIGHT * 1
SR37x_ERRWEIGHT     * 4
SR37x_ERRMAXLIMIT   * 2

; TPS65950 parameters
TPS_VP_ERROROFFSET      * 0
TPS_VP_VSTEPMIN         * 1
TPS_VP_VSTEPMAX         * 4
TPS_VP_VLIMITTO_TIMEOUT * 200
TPS_VP1_VLIMITTO_VDDMIN * &18
TPS_VP1_VLIMITTO_VDDMAX * &3C
TPS_VP2_VLIMITTO_VDDMIN * &18
TPS_VP2_VLIMITTO_VDDMAX * &30
TPS_VP_STEPSIZE         * 12500 ; uV
TPS_VP_SLEWRATE         * 4000 ; uV/uS

ABB_SETTLING_TIME       * 30 ; uS
ABB_FBB_VOLTAGE         * 54 ; 1.27V. Enable FBB for anything above this.

; OPP table format for the SmartReflex driver
                              ^  0
SR37x_OPPTbl_MHz              #  2 ; Max MHz value for this voltage
SR37x_OPPTbl_VDD              #  1 ; Required VDD value, in VDD1_VSEL format
SR37x_OPPTbl_CLKOUT_M2        #  1 ; Required DPLL1CLKOUT_M2 value
SR37x_OPPTbl_NVALUERECIPROCAL #  4 ; Required NVALUERECIPROCAL value
SR37x_OPPTbl_ERRCONFIG        #  3 ; Required SR ERRCONFIG value (bits 0-18)
SR37x_OPPTbl_ERRGAIN          #  1 ; Required VP ERRGAIN value
SR37x_OPPTbl_Size             #  0 ; Size of each entry

SR37x_OPPTbl_Format    *  0 ; Format number as returned by SR37x_Override

SR37x_OPPTbl_Max       *  6 ; Max number of entries we support

; Workspace
                   ^  0, a1
; Public bits
SR37xDevice       #  HALDevice_CPUClk_Size_0_2 ; support API 0.2
; Private bits
SR37xShutdown     #  4 ; Pointer to shutdown func. Must be at same offset as CPUClkShutdown!
SR37xWorkspace    #  4 ; HAL workspace pointer
SR37xNewSpeed     #  4 ; Re-entrancy flag. -1 if idle, desired table idx if in process of changing CPU speed. Allows SR37x_Get to finish changing the speed if it gets called in the middle of a change. (Note somewhat redundant since SR37x code currently avoids re-entrancy)
SR37xCurSpeed     #  4 ; Current table idx
SR37xOPPTblSize   #  4 ; Number of entries in table
SR37xOPPTbl       #  SR37x_OPPTbl_Size*SR37x_OPPTbl_Max ; OPP table
SR37x_DeviceSize  *  :INDEX: @

SR37x_WorkspaceSize  * SR37x_DeviceSize

; Smartreflex driver workspace fits into CPUClk workspace
        ASSERT    SR37x_WorkspaceSize <= CPUClk_WorkspaceSize


        END
